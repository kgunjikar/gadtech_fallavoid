import pandas as pd
import numpy as np
import pdb
import glob, os
import matplotlib.pyplot as plt

path = '../falldetect_dataset1/'
all_files = glob.glob(os.path.join(path, "2*.csv"))

print 'Using files' + str(all_files)

df = pd.concat(pd.read_csv(f, header=None, usecols=[1,2,3,4], names=["X","Y","Z","Label"]) for f in all_files) 
# Get training set and dataset
msk = np.random.rand(len(df)) < 0.8

train = df[msk]
test = df[~msk]


# Find features
'''
print train['X'].groupby(train['Label']).describe()
print train['Y'].groupby(train['Label']).describe()
print train['Z'].groupby(train['Label']).describe()
'''
try:
    plt.figure()
    tempx = train['X'].groupby(train['Label'])
    tempx.plot()
    tempy = train['Y'].groupby(train['Label'])
    tempy.plot()
    tempz = train['Z'].groupby(train['Label'])
    tempz.plot()
    plt.show()
except:
    print 'Unable to plot data'
