from kivy.uix.gridlayout import GridLayout
from kivy.app import App
from kivy.lang import Builder
from plyer import accelerometer 
from kivy.uix.label import Label
from kivy.storage.jsonstore import JsonStore
from kivy.clock import Clock

Builder.load_file('boxlayout.kv')

class FallDetection(GridLayout):
    def __init__(self, **kwargs):
        super(FallDetection, self).__init__(**kwargs)
        self.lblAccel = Label(text='Accelrometer:')
        self.add_widget(self.lblAccel)
        self.defaultstore = JsonStore('default.json')
        self.label = 'None'
        self.count = 0
        #try:
            #accelerometer.enable()
        Clock.schedule_interval(self.update, 1.0/24)
        #except:
        #     self.lblAccel.text ="Failled to init accelrometer"

    def walking(self, **kwargs):
        self.label = 'walking'
        self.count = 0

    def standing(self, **kwargs):
        self.label = 'standing'
        self.count = 0

    def running(self, **kwargs):
        self.label = 'running'
        self.count = 0

    def steady(self, **kwargs):
        self.label = 'steady'
        self.count = 0

    def sitting(self, **kwargs):
        self.label = 'sitting'
        self.count = 0

    def falling(self, **kwargs):
        self.label = 'falling'
        self.count = 0


    def update(self, dt):
        txt=""
        try:
            txt = "Accelerometer:\n X = %.2f\n Y = %.2f\n Z = %.2f " %(
                   accelerometer.acceleration[0],
                   accelerometer.acceleration[1],
                   accelerometer.acceleration[2]
                   )
            self.count += 1
            store.put(str(self.count),value=txt,label=self.label)
        except:
            txt = "Cannot read accelerometer"
        self.lblAccel.text = txt


class FallDetectionApp(App):
    def build(self):
        return FallDetection()


if __name__ == '__main__':
    FallDetectionApp().run()


