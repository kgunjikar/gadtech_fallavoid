"""
  Training the KNN classifier to detect activity. 
  Two approaches 
   1. a = sqrt(ax^2 + ay ^2 + az^2) and label 
   2. or ax, ay, az and the label 
"""
from sklearn.neighbors import KNeighborsClassifier
import csv 
import numpy as np

training_files=["./UCI HAR Dataset/train/X_train.txt"]


def sq(x):
  return np.square(float(x))

def dist(x, y, z):
  return (np.sqrt(sq(float(x))+sq(float(y))+sq(float(z))))

def test(testfile, approach_no, knn, logfile):
  correct = 0
  wrong = 0
  testlabel = []
  testing = []
  predicted = []

  fp = open(testfile,"r");
  count = 0
  testreader = csv.reader(fp,delimiter=',')
  for row in testreader:
    count = count + 1
    testlabel.append(row[4])
    
    testing = []

    if approach_no == 1 : 
      temp = dist(row[1], row[2], row[3])
      testing.append(temp)
    else :
      testing.append(row[1])
      testing.append(row[2])
      testing.append(row[3])

    val = knn.predict(testing)
    predicted.append(val)
    if val[0] == row[4] :
      correct = correct + 1 
      #logfile.write('Correct ' + str( val[0]) + str (row[4]) + '\n')
    else:
      wrong = wrong + 1 
      logfile.write('#' + str(count) + 'Wrong Predicted:' + str( val[0])+"Actual:" + str (row[4])+ '\n')

  total = wrong + correct
  print 'Right:'+ str(correct) +'Wrong:' +str(wrong)+'Total:'+str(correct+wrong)
  print 'Accuracy:' + str(float(float(correct)/total) * 100)
  logfile.write('Right:'+ str(correct) +'Wrong:' +str(wrong)+'Total:'+str(correct+wrong))


def train(approach_no):
  traindata = []
  label = []

  count = 0
  for trainfile in training_files:
    print "Training using file:" + str(trainfile)
    fp = open(trainfile,"r");

    extension = trainfile.split('.')
    if extension[2] == 'txt':
      sep = ' '
    else:
      sep = ','
    trainreader = csv.reader(fp,delimiter=sep)
    for row in trainreader:
      temp = []
      label.append(row[4])
      # Approach 1 
      if approach_no == 1 :
        accel= dist(row[1], row[2], row[3])
        temp.append(accel)
      else:
      # Approach 2 
      # Use only 30,000 label samples for 
      # training rest are for testing
        temp.append(row[1])
        temp.append(row[2])
        temp.append(row[3])
      traindata.append(temp)
      count = count + 1
  
   
  print "Total Training samples: " + str(count) 
  knn = KNeighborsClassifier(n_neighbors=23)
  knn.fit(traindata, label)    
  return (knn)
    
def main():
  logfile = open("log.txt","w")
  knn = train(1);
  print "Training done"
  test('10.csv', 1, knn, logfile);

if __name__=='__main__':                                                                                 
  main()                                                                                                 
