library('ggplot2')
library('plyr')
library('reshape')
library('caret')

ProjectDirectory = getwd()
DataDirectory = "UCI HAR Dataset/"
dataFile = "dataset.RData"
if (!file.exists(DataDirectory)) {
    download.file("https://archive.ics.uci.edu/ml/machine-learning-databases/00240/UCI%20HAR%20Dataset.zip", 
        "data.zip", "curl", quiet = TRUE, mode = "wb")
    unzip("data.zip")
    file.remove("data.zip")
}
stopifnot(file.exists(DataDirectory))
setwd(DataDirectory)
if (!file.exists(dataFile)) {
    temp = read.table("activity_labels.txt", sep = "")
    activityLabels = as.character(temp$V2)
    temp = read.table("features.txt", sep = "")
    attributeNames = temp$V2

    Xtrain = read.table("train/X_train.txt", sep = "")
    names(Xtrain) = attributeNames
    Ytrain = read.table("train/y_train.txt", sep = "")
    names(Ytrain) = "Activity"
    Ytrain$Activity = as.factor(Ytrain$Activity)
    levels(Ytrain$Activity) = activityLabels
    trainSubjects = read.table("train/subject_train.txt", sep = "")
    names(trainSubjects) = "subject"
    trainSubjects$subject = as.factor(trainSubjects$subject)
    train = cbind(Xtrain, trainSubjects, Ytrain)

    Xtest = read.table("test/X_test.txt", sep = "")
    names(Xtest) = attributeNames
    Ytest = read.table("test/y_test.txt", sep = "")
    names(Ytest) = "Activity"
    Ytest$Activity = as.factor(Ytest$Activity)
    levels(Ytest$Activity) = activityLabels
    testSubjects = read.table("test/subject_test.txt", sep = "")
    names(testSubjects) = "subject"
    testSubjects$subject = as.factor(testSubjects$subject)
    test = cbind(Xtest, testSubjects, Ytest)

    save(train, test, file = dataFile)
    rm(train, test, temp, Ytrain, Ytest, Xtrain, Xtest, trainSubjects, testSubjects, 
        activityLabels, attributeNames)
}
load(dataFile)
setwd(ProjectDirectory)
numPredictors = ncol(train) - 2


summary(train$subject)


summary(test$subject)


train$Partition = "Train"
test$Partition = "Test"
all = rbind(train, test)  # combine sets for visualization
all$Partition = as.factor(all$Partition)
qplot(data = all, x = subject, fill = Partition)


qplot(data = all, x = subject, fill = Activity)


rm(all)  # recover memory


trainSd = colwise(sd)(train[, 1:numPredictors])
trainSd$stat = "Predictor Variable Standard Deviation"
trainMean = colwise(mean)(train[, 1:numPredictors])
trainMean$stat = "Predictor Variable Mean"
temp = melt(rbind(trainMean, trainSd), c("stat"))
qplot(data = temp, x = value, binwidth = 0.025) + facet_wrap(~stat, ncol = 1)


rm(temp, trainMean, trainSd)


zScaleTrain = preProcess(train[, 1:numPredictors])
scaledX = predict(zScaleTrain, train[, 1:numPredictors])
head(names(scaledX))


nzv = nearZeroVar(scaledX, saveMetrics = TRUE)
summary(nzv)


head(nzv[order(nzv$percentUnique, decreasing = FALSE), ], n = 20)

head(nzv[order(nzv$freqRatio, decreasing = TRUE), ], n = 20)


correlatedPredictors = findCorrelation(cor(scaledX), cutoff = 0.95)


reducedCorrelationX = scaledX[, -correlatedPredictors]
head(names(reducedCorrelationX))


pcaTrain = preProcess(scaledX, method = "pca", thresh = 0.99)


pcaX = predict(pcaTrain, scaledX)
head(names(pcaX))


leaveOneSubjectOutIndices = lapply(levels(train$subject), function(X) {
    which(!X == train$subject)
})


cvBreaks = 7
temp = sample(levels(train$subject), length(levels(train$subject)))  # randomize subjects
temp = split(temp, cut(1:length(temp), breaks = cvBreaks, labels = FALSE))  # split into CV groups
cvGroupIndices = lapply(temp, function(X) {
    which(!train$subject %in% X)
})


library(parallel)
cl = parallel::makeForkCluster(nnodes = ceiling(detectCores()/2))
setDefaultCluster(cl)
library(doParallel)


registerDoParallel(cl)
getDoParWorkers()


saveFile = paste(DataDirectory, "modelRF.RData", sep = "")
if (!file.exists(saveFile)) {
    rfCtrl = trainControl(method = "cv", number = length(cvGroupIndices), index = cvGroupIndices, 
        classProbs = TRUE)
    modelRF = train(reducedCorrelationX, train$Activity, method = "rf", trControl = rfCtrl, 
        tuneGrid = data.frame(.mtry = c(2, 5, 10, 15, 20)), importance = TRUE)
    save(rfCtrl, modelRF, correlatedPredictors, zScaleTrain, file = saveFile)
}
if (!exists("modelRF")) {
    load(saveFile)
}
print(modelRF)


plot(modelRF)

print(confusionMatrix(modelRF))

m = as.data.frame(modelRF$finalModel$importance)
m = m[order(m$MeanDecreaseAccuracy, decreasing = TRUE), ]
head(m, n = 20)

saveFile = paste(DataDirectory, "modelParRF.RData", sep = "")
if (!file.exists(saveFile)) {
    parRfCtrl = trainControl(method = "cv", number = length(cvGroupIndices), index = cvGroupIndices, 
        classProbs = TRUE)
    modelParRF = train(reducedCorrelationX, train$Activity, method = "parRF", trControl = parRfCtrl, 
        tuneGrid = data.frame(.mtry = c(2, 5, 10, 15, 20)), importance = TRUE)
    save(parRfCtrl, modelParRF, correlatedPredictors, zScaleTrain, file = saveFile)
}
if (!exists("modelParRF")) {
    load(saveFile)
}
print(modelParRF)


plot(modelParRF)


print(confusionMatrix(modelParRF))


saveFile = paste(DataDirectory, "modelKnn.RData", sep = "")
if (!file.exists(saveFile)) {
    knnCtrl = trainControl(method = "cv", number = length(cvGroupIndices), index = cvGroupIndices, 
        classProbs = TRUE)
    modelKnn = train(reducedCorrelationX, train$Activity, method = "knn", trControl = knnCtrl, 
        tuneGrid = data.frame(.k = c(5, 10, 15, 20)))
    save(knnCtrl, modelKnn, correlatedPredictors, zScaleTrain, file = saveFile)
}
if (!exists("modelKnn")) {
    load(saveFile)
}
print(modelKnn)


confusionMatrix(modelKnn)

bestModel = modelRF


holdoutX = predict(zScaleTrain, test[, 1:numPredictors])[, -correlatedPredictors]
holdoutLabels = test$Activity
holdoutPrediction = predict(bestModel, holdoutX)


head(holdoutPrediction)


classProbPrediction = predict(bestModel, holdoutX, type = "prob")
head(classProbPrediction)

holdoutConfusionMatrix = confusionMatrix(holdoutPrediction, holdoutLabels)


print(holdoutConfusionMatrix)

print(confusionMatrix(bestModel), digits = 2)


print(100 * (holdoutConfusionMatrix$table/sum(holdoutConfusionMatrix$table)), digits = 1)


save.image(file = "workspaceImage.RData")


