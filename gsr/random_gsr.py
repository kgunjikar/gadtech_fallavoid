#!/usr/bin/python -tt 

"""
  Training the Random Forest classifier to detect activity. 
"""
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.lda import LDA
import csv 
import numpy as np
import sys

trainfiles=["Dimitry_Standing.txt"]
testfiles=["Dimitry_Standing.txt"]

def test(classifier, data, label):
  count = 0
  correct = 0
  wrong = 0
  while count < len(label):
    predict = classifier.predict(data[count])
    if (predict == label[count]) :
      correct = correct + 1
    else:
      wrong = wrong + 1 
    count = count + 1
  return wrong, correct 

def read_gsr_files(files):
  data = [] 
  label = [] 
  acc = 0
  ten = 10.0
  testfp = open('test','w')
  for filename in files:
    fp = open(filename,"r")
    for line in fp:
      x = line.split()
      acc =  float(x[0])
      temp = []
      for item in x[1:] :
        temp.append(float(item))
      data.append(temp)
      label.append(int(ten))
      if int(acc - ten) == 0.0 :
        ten += 10.0 
  for val1, val2 in zip(data, label):
      testfp.write(str([val1, val2]))
  return data, label

#Zero Crossing
def zero_cross(X):
  s= numpy.sign(X)  
  s[s==0] = -1     # replace zeros with -1  
  zero_crossings = numpy.where(numpy.diff(s))[0]  
  return (zero_crossings)

#Root mean square
def rms(X):
  return (np.sqrt(np.mean(np.square(X))))

#Mean absolute value  
def mav(X):
  return(np.mean(np.absolute(X)))

#Integrated absolute value 
def iav(X):
  return(np.sum(np.absolute(X)))
 
def find_energy(X):
# Based of Parseval's theorem 
  y = np.fft.fft(X)
  return(np.sum(np.abs(y) ** 2) / len(X))

def find_features(data, features):
  temp = []
  mean = np.mean(np.absolute(data))
  iemg = np.sum(np.absolute(data))
  integ_av = iav(data)
  r_mean_sq = rms(data)
  movingav = mav(data)
  temp.extend([mean, iemg, integ_av, r_mean_sq, movingav])
  features.append(temp)

def extract_features(data, label):
  window = 0
  fdata= []
  flabel = [] 
  while window < len(data) :
    if window + 10 > len(data) :
      size = len(data) - 1
    else: 
      size = 10
    x = np.array(data[window:window+size])
    z = x.transpose()
    find_features(z,fdata)
    flabel.append(label[size])
    window = window + size
  return fdata, flabel

def train(traindata, trainlabel):
  lda_classify = LDA()
  lda_classify.fit(traindata, trainlabel)
  return classifier

if __name__=='__main__': 
  traindata = []
  trainlabel = []
  testdata = []
  testlabel = []
  ftraindata=ftrainlabel=ftestdata=ftestlabel = [] 
  traindata, trainlabel = read_gsr_files(trainfiles)
  if len(trainlabel) != len(traindata) :
    print 'Whaat!'
  testdata, testlabel = read_gsr_files(testfiles)
  if len(testlabel) != len(testdata) :
    print 'Whaat!'

  ftraindata, ftrainlabel = extract_features(traindata,trainlabel)
  ftestdata, ftestlabel = extract_features(testdata,testlabel)
  print len(ftestdata), len(ftestlabel), len(ftrainlabel),len(ftraindata)

  classifier = train(ftraindata, ftrainlabel)
  wrong, correct = test(classifier, ftestdata, ftestlabel)
  accuracy = (float)(correct * 1.0/len(testlabel))
  print accuracy 
