# include <stdio.h>

FILE *fp1, *fp2, *output;

main()
{
    float sensor_val = 0.0;
    int label = 0;

    fp1 = fopen("test/X_test.txt","r");
    if (fp1 == NULL) { 
        printf("\n Error opening file XTrain");
        goto done;
    }
    fp2 = fopen("test/y_test.txt","r");
    if (fp2 == NULL) { 
        printf("\n Error opening file yTrain");
        goto done;
    }
    output = fopen("test/output.txt","w");
    if (output == NULL) { 
        printf("\n Error opening file Output");
        goto done;
    }
    for(;!feof(fp1) && !feof(fp2);) {
       fscanf(fp1, "%f", &sensor_val);
       fscanf(fp2, "%d", &label);
       printf("\n Sensor val: %f Label: %d", sensor_val, label);
       fprintf(output,"\n %f %d", sensor_val, label);
    }
done:
    if (fp1) { 
        fclose(fp1);
    } 
    if (fp1) { 
        fclose(fp2);
    }
    if (output) { 
        fclose(output);
    }
}
