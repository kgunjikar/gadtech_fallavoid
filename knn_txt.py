"""
  Training the KNN classifier to detect activity. 
"""
from sklearn.neighbors import KNeighborsClassifier
import csv 
import numpy as np

training_files = ["./UCI HAR Dataset/train/output.txt"]
testing_files = ["./UCI HAR Dataset/test/output.txt"]

def read_file(datafile, count):
    print "Using file:" + str(datafile)
    fp = open(datafile,"r");

    data = []
    label = []
    reader = fp.readlines()
    for row in reader:
      dataset = row.split(" ")
      temp = []
      label.append(int(dataset[2]))
      temp.append(float(dataset[1]))
      data.append(temp)
      count = count + 1
    """ 
    print data
    raw_input("Check")
    print label
    raw_input("Check")
    print count
    raw_input("Check")
    """
    return data, label, count


def test(knn, logfile):
  correct = 0
  wrong = 0
  count = 0
  for testfile in testing_files:
    testing, label, count = read_file(testfile, count)     
  print "Total Testing samples: " + str(count) 

  predictions = []
  for testval in testing:
    prediction = knn.predict(testval)
    predictions.append(prediction)
    
  print predictions
  for (val,predict) in zip(label, predictions):
    if val == predict :
      correct = correct + 1;
    else: 
      wrong = wrong + 1;

  print correct, wrong, count

def train():
  traindata = []
  label = []

  count = 0
  for trainfile in training_files:
    traindata, label, count = read_file(trainfile, count)     

  print "Total Training samples: " + str(count) 
  knn = KNeighborsClassifier(n_neighbors=23)
  knn.fit(traindata, label)    
  return (knn)
    
def main():
  logfile = open("log.txt","w")
  knn = train();
  print "Training done"
  test(knn, logfile);

if __name__=='__main__':                                                                                 
  main()                                                                                                 
