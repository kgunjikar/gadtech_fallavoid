"""
  Training the KNN classifier to detect activity. 
  Two approaches 
   1. a = sqrt(ax^2 + ay ^2 + az^2) and label 
   2. or ax, ay, az and the label 
"""
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
import csv 
import numpy as np

training_files = ["./UCI HAR Dataset/train/output.txt"]

def read_file(datafile, count):
    print "Using file:" + str(datafile)
    fp = open(datafile,"r");

    data = []
    label = []
    reader = fp.readlines()
    for row in reader:
      dataset = row.split(" ")
      temp = []
      label.append(int(dataset[2]))
      temp.append(dataset[1])
      data.append(temp)
      count = count + 1
    """ 
    print data
    raw_input("Check")
    print label
    raw_input("Check")
    print count
    raw_input("Check")
    """
    return data, label, count

def main():
   count = 0
   data, label, count = read_file(training_files[0], count)
   
   rfc = RandomForestClassifier(n_estimators=100)
   rfc.fit(data, label)

   scores = cross_val_score(rfc, data, label, cv=100)
   print("Accuracy: %0.2f (+/- %0.2f)"
      % (scores.mean(), scores.std()*2))

if __name__=='__main__':                                                                                 
  main()                                                                                                 
